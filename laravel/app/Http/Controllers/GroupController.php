<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Course;
use App\Models\Group;
use App\Models\Kafedra;


class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dev = json_decode($request->groupdata);
        $kaf_id = Kafedra::where('user_id',auth()->user()->id)->max('id');
        if(!isset($kaf_id)){
        return response()->json([
            'error'=>"your kafedra name is not found ! "
        ]);
       }
        foreach ($dev as $key => $value) {
            $obj = new Group();
            $obj->name = $value->name;
            if($value->kurs == "1-Kurs")
            $obj->course_id = 1;
            elseif($value->kurs == "2-Kurs")
            $obj->course_id = 2;
            elseif($value->kurs == "3-Kurs")
            $obj->course_id = 3;
            elseif($value->kurs == "4-Kurs")
            $obj->course_id = 4;
            else return response()->json([
                'errors'=>'Kurs is no directory'
            ]);
            $obj->kafedra_id = $kaf_id;
            $obj->save();
        }
        return response()->json([
            'msg'=>"group's name save Successfully ! "
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getCourse(){
        $courses = Course::all();
        return response()->json($courses);
    }
}
