<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Group;
use App\Models\Kafedra;
use App\Models\LectureJoin;


class LectureJoinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kaf_id = Kafedra::where('user_id',auth()->user()->id)->max('id');
        if(!isset($kaf_id)){
        return response()->json([
            'error'=>"your kafedra name is not found ! "
        ]);
       }
        $kurs1 = json_decode($request->kurs1);
        $kurs2 = json_decode($request->kurs2);
        $kurs3 = json_decode($request->kurs3);
        $kurs4 = json_decode($request->kurs4);

      foreach($kurs1 as $ku1){  
        $obj1=new LectureJoin;    
        $obj1->subject_id=$ku1->subject_id;
        $obj1->kafedra_id=$kaf_id;
        $obj1->course_id=1;
        $obj1->save();
        $obj1->group()->attach($ku1->group_id);
      }

      foreach($kurs2 as $ku2){
        $obj2=new LectureJoin;    
        $obj2->subject_id=$ku2->subject_id;
        $obj2->kafedra_id=$kaf_id;
        $obj2->course_id=2;
        $obj2->save();
        $obj2->group()->attach($ku2->group_id);
     }

     foreach($kurs3 as $ku3){
        $obj3=new LectureJoin;    
        $obj3->subject_id=$ku3->subject_id;
        $obj3->kafedra_id=$kaf_id;
        $obj3->course_id=3;
        $obj3->save();
        $obj3->group()->attach($ku3->group_id); 
     }
     foreach($kurs4 as $ku4){
        $obj3=new LectureJoin;    
        $obj3->subject_id=$ku4->subject_id;
        $obj3->kafedra_id=$kaf_id;
        $obj3->course_id=4;
        $obj3->save();
        $obj3->group()->attach($ku4->group_id); 
     }

      return "yeeah !";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getGroup(Request $request){

        // $request->validate([
        //     'id'=>['required','numeric']
        // ]);
        $kafed_id = Kafedra::where('user_id',auth()->user()->id)->max('id');
        if(!isset($kafed_id)){
        return response()->json([
            'error'=>"your kafedra name is not found ! "
        ]);
       }
       $kurs1 = Group::where('kafedra_id',$kafed_id)
        ->where('course_id',1)
        ->get();

        $kurs2 = Group::where('kafedra_id',$kafed_id)
        ->where('course_id',2)
        ->get();

        $kurs3 = Group::where('kafedra_id',$kafed_id)
        ->where('course_id',3)
        ->get();

        $kurs4 = Group::where('kafedra_id',$kafed_id)
        ->where('course_id',4)
        ->get();

        return response()->json([
            'kurs1'=>$kurs1,
            'kurs2'=>$kurs2,
            'kurs3'=>$kurs3,
            'kurs4'=>$kurs4 
        ]);
    }
}
