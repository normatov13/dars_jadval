<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Group;
use App\Models\Kafedra;
use App\Models\GroupSubject;

class GroupSubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kaf_id = Kafedra::where('user_id',auth()->user()->id)->max('id');
        if(!isset($kaf_id)){
        return response()->json([
            'error'=>"your kafedra name is not found ! "
        ]);
       }
        $req = json_decode($request->data);
         foreach($req as $re){
                 foreach($re->subject_id as $r){
                 GroupSubject::create([
                     'group_id'=>$re->group_id,
                     'subject_id'=>$r,
                     'kafedra_id'=> $kaf_id,
                     'l_time'=>$re->l_time,
                     'p_time'=>$re->p_time
                 ]);     
                 }
         }
         return response()->json([
             'msg'=>"Data saved successfully !"
         ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getGroup(){
        $kaf_id = Kafedra::where('user_id',auth()->user()->id)->max('id');
        if(!isset($kaf_id)){
        return response()->json([
            'error'=>"your kafedra name is not found ! "
        ]);
       }
        $all = Group::where('kafedra_id',$kaf_id)->get();
        return response()->json($all);
    }
}
