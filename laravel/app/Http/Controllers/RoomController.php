<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Room;
use App\Models\Kafedra;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kaf_id = Kafedra::where('user_id',auth()->user()->id)->max('id');
        if(!isset($kaf_id)){
        return response()->json([
            'error'=>"your kafedra name is not found ! "
        ]);
       }
        $practise=json_decode($request->practical);
        $lecture=json_decode($request->lecture);
        foreach($practise as $prac){
            Room::insert([
                'name'=>$prac->p_name,
                'type'=>0,
                'kafedra_id'=>$kaf_id
            ]);
        }
        foreach($lecture as $lec){
            Room::insert([
                'name'=>$lec->l_name,
                'type'=>1,
                'kafedra_id'=>$kaf_id
            ]);
        }
        return response()->json([
            'msg'=>"Rooms save successfully"
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
