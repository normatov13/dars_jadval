<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Group;
use App\Models\Kafedra;
use App\Models\Subject;
use App\Models\Room;
use App\Models\Day;
use App\Models\Teacher;
use App\Models\LectureJoin;
use App\Models\GroupSubject;
use App\Models\TeacherDivision;
use App\Models\TeacherSubject;

class MixTableController extends Controller
{
    protected $id, $groups, $rooms,$teachers,$lectureJoin,$days,$grSubjects ;


   /* 
     @ get data for mix table Fully 
   */
    public function __construct(){
        $this->getKafedraId();
        $this->getday();
        $this->getGroup();
        $this->getRoom();
        $this->getTeacher();
        $this->getLectureJoin();
        $this->getGroupSubject();
        // $this->getTeacherSubject();
    } 


    protected function getKafedraId(){
            if(isset(auth()->user()->id)){
              $this->id = Kafedra::where('user_id',auth()->user()->id)->max('id');
            }else{
                die('Please token expired !');
            }
    }
    protected function getday(){
        $this->days = Day::all();
    }
    protected function getGroup(){
           $this->groups = Group::where('kafedra_id',$this->id)->get();
    }
    protected function getRoom(){
        $this->rooms = Room::where('kafedra_id',$this->id)->get();
    }
    protected function getTeacher(){
        $this->teachers = Teacher::where('kafedra_id',$this->id)->with('subject')->get();
    }
    protected function getLectureJoin(){
        $this->lectureJoin = LectureJoin::where('kafedra_id',$this->id)->get();
    }
    protected function getGroupSubject(){
        $this->grSubjects = GroupSubject::where('kafedra_id',$this->id)->get();
    }
    protected function getCountTime(){
       $datas = TeacherDivision::where('kafedra_id',$this->id)->get();
      foreach($datas as $data){
      }
    }
    // protected function getTeacherSubject(){
    //     $this->grSubjects = TeacherSubject::where('kafedra_id',$this->id)->get();
    // }
    public function index(){
        
        foreach($this->days as $day_i=>$day){
            for($j=1;$j<5;$j++){
                 foreach($this->groups as $gr_i=>$guruh){
                  
                  $fanlar[$day_i][$j][$gr_i] = '';


               }
           }
        }
   
    //  dd($fanlar);

    }
}
