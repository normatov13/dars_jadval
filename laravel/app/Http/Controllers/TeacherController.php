<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Subject;
use App\Models\Teacher;
use App\Models\Kafedra;
use App\Models\TeacherSubject;
use App\Models\TeacherDivision;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kaf_id = Kafedra::where('user_id',auth()->user()->id)->max('id');
        if(!isset($kaf_id)){
        return response()->json([
            'error'=>"your kafedra name is not found ! "
        ]);
       }
        $data = json_decode($request->teachers);
            foreach($data as $obj){
                Teacher::create([
                    'full_name'=>$obj->name,
                    'kafedra_id'=>$kaf_id
                ]);
            }
            return response()->json([
                'msg'=>'Teacher\'s full name save successful'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getSubject(){
     $data = Subject::all()->sortBy(['s_name', 'asc'],);
     return response()->json($data);
    }

    public function saveShare(Request $request){
          return $request;
        $kaf_id = Kafedra::where('user_id',auth()->user()->id)->max('id');
        if(!isset($kaf_id)){
        return response()->json([
            'error'=>"your kafedra name is not found ! "
        ]);
       }
           $datas = json_decode($request->data);
          foreach($datas as $data1){
                foreach($data1->share as $share){
                   $objTech =new TeacherSubject;
                   $objTech->teacher_id=$data1->teacher_id;
                   $objTech->subject_id=$share->subject_id;
                   $objTech->save();
                   foreach($share->courseTime as $shar){
                       
                    TeacherDivision::create([
                        'teacher_subject_id'=>$objTech->id,
                        'course_id'=>$shar->course_id,
                        'kafedra_id'=>$kaf_id,
                        'l_time'=>$shar->l_time,
                        'p_time'=>$shar->p_time
                    ]);   
                   }
                }
          } 
        return response()->json([
            'msg'=>'work divisons save succesfully!'
        ])  ;
    }
}
