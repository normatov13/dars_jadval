<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupSubject extends Model
{
    use HasFactory;
    
    protected $table='group_subject';
  
    protected $fillable=['group_id','subject_id','kafedra_id','l_time','p_time'];

}
