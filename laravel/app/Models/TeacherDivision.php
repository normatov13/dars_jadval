<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeacherDivision extends Model
{
    use HasFactory;
    protected $fillable = ['teacher_subject_id','course_id','l_time','p_time'];
}
