<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LectureJoin extends Model
{
    use HasFactory;

    protected $fillable = ['group_id','subject_id','kafedra_id','course_id'];
    
    public function group(){
        return $this->belongsToMany(Group::class,'lecture_join_group','lecture_join_id','group_id');
    }

}
