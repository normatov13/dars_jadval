<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    use HasFactory;
    protected $fillable = ['full_name','kafedra_id'];

    public function subject(){
        return $this->belongsToMany(Subject::class,'teacher_subject');
    }
}
