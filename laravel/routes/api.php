<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' => 'api',
    'namespace' => 'App\Http\Controllers',
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

Route::group([
    'middleware' => 'jwt',
    'namespace' => 'App\Http\Controllers',
    'prefix'=>'create'
], function($router) {
    Route::post('kafedra', 'KafedraController@store');   
    Route::get('getCourse', 'GroupController@getCourse');   
    Route::post('group', 'GroupController@store');   
    Route::get('getSubject', 'TeacherController@getSubject');   
    Route::post('teacher', 'TeacherController@store'); 
    Route::post('saveShares', 'TeacherController@saveShare');  
    Route::post('room','RoomController@store'); 
    Route::get('getGroup', 'LectureJoinController@getGroup');  
    Route::post('joinLecture', 'LectureJoinController@store');  
    Route::get('getGroupforS', 'GroupSubjectController@getGroup');  
    Route::post('getTeacher', 'TeacherWorkShare@getTeacher');  
    Route::post('saveGroupSubject', 'GroupSubjectController@store');  
    Route::get('mixTableCon', 'MixTableController@index');  
});
