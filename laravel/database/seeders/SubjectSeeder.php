<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            "Matematik Analiz",
            "Dasturlash",
            "Kriptografiya",
            "Nazariy Mexanika",
            "Jismoniy Tarbiya",
            "Psixalogiya",
            "Tarix",
            "Ona Tili O'qitish metodikasi",
            "Falsafa",
            "Kompyuter grafikasi",
            "Matematik Modellashtirish"
        ];
        
        foreach($data as $da){
            DB::table('subjects')->insert(["s_name"=>$da]);
        }
    }
}
