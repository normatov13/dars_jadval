<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses')->insert([
          ['name' => '1-Kurs'],
          [ 'name' => '2-Kurs'],
           ['name' => '3-Kurs'],
           ['name' =>'4-Kurs']
        ]);
    }
}
