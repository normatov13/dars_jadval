<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class DaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('days')->insert([
            ['name'=>'Dushanba'],
            ['name'=>'Seshanba'],
            ['name'=>'Chorshanba'],
            ['name'=>'Payshanba'],
            ['name'=>'Juma'],
            ['name'=>'Shanba'],
        ]);
    }
}
