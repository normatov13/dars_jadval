<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeacherDivisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher_divisions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('teacher_subject_id');
            $table->foreignId('kafedra_id')->constrained();
            $table->foreignId('course_id');
            $table->integer('l_time')->default(0);
            $table->integer('p_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher_divisions');
    }
}
