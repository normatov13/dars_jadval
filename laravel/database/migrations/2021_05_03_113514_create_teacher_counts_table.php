<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeacherCountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher_counts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('subject_id');
            $table->foreignId('kafedra_id')->constrained();
            $table->foreignId('course_id');
            $table->integer('tc_lecture')->nullable();
            $table->integer('tc_practise')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher_counts');
    }
}
