import {LocalStorage,Loading} from 'quasar'
import { axiosTwo } from 'boot/axios'


export async function login ({commit,dispatch},payload) { 

    await axiosTwo.post('/api/auth/login', { ...payload }).then(async response => {
     let token = response.data
     LocalStorage.set('token',token,{ expires: token.remember ? token.expires_in : null });
     this.$router.push('/').catch(() => { })

    })
}