
const routes = [
  {
   path: '/login',
   component: () => import('layouts/loginLayout.vue')
  },
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/new/groups', component: () => import('pages/data/new.groups.vue'),name:'New Groups'  },
      { path: '/new/kafedra', component: () => import('pages/data/new.kafedra.vue'),name:'New Kafedra' },
      { path: '/new/teachers', component: () => import('pages/data/new.teachers.vue'),name:'New Teachers' },
      { path: '/new/teachers_share', component: () => import('pages/data/new.teachers.share.vue'),name:'Teacher\'s work Share' },
      { path: '/new/rooms', component: () => import('pages/data/new.rooms.vue'),name:'New Rooms' },
      { path: '/new/lecture_join_group', component: () => import('pages/data/new.lecture_join.vue'),name:'Lecture Joins' },
      { path: '/new/groups_subjects', component: () => import('pages/data/new.groups_subject.vue'),name:'Group\'s Subjects' },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
