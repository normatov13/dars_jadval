import { Notify, LocalStorage } from 'quasar'
import Vue from 'vue'
import { i18n } from './i18n'
import axios from 'axios'
import router from '../router'
import store from '../store'

// We create our own axios instance and set a custom base URL.
// Note that if we wouldn't set any config here we do not need
// a named export, as we could just `import axios from 'axios'`
const axiosTwo = axios.create({ baseURL: process.env.API_URL})

axiosTwo.interceptors.request.use(request => {
    const token = LocalStorage.getItem('token')
  
    if (token) request.headers.common.Authorization = `Bearer ${token.access_token}`
    return request
})


export default async ({ router, store, Vue }) => {
    Vue.prototype.$axios = axiosTwo
}

export { axiosTwo }
